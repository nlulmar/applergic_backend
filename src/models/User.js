const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
      name: {type: String},
      mail: {type: String, required: true},
      password: {type: String, required: true},
      phoneNumber: {type: Number},
      picture: {type: URL},
      allergens: {type: mongoose.Types.ObjectId, ref: 'allergens'},
      favorites: {type: mongoose.Types.ObjectId, ref: 'foods'},
      emergencyContact: {
        name: {type: String},
        phoneNumber: {type: Number},
        mail: {type: String},
        insurance: {
          company: {type: String},
          policyId: {type: String},
        },
      },
    },
    {
      timestamps: true,
    },
);

const User = mongoose.model('User', userSchema);
module.exports = User;
