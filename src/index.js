require('dotenv').config();
require('./db.js');
const express = require('express');

const server = express();
// eslint-disable-next-line new-cap
const router = express.Router();
const PORT = process.env.PORT || 3000;

server.use('/', router);

server.listen(PORT, () => {
  console.log(`Server is listenind in http://localhost:${PORT}`);
});
